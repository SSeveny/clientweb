/*------------------------------------------------------------------------
   File        : ClientResidenciesOBPWebHandler
   Purpose     : Web handlers for managing client residency records
   Syntax      :
   Description :
   Author(s)   : lauries
   Created     : Feb 15, 2021
   Notes       :
 ----------------------------------------------------------------------*/
using OpenEdge.Web.IWebRequest from propath.
using OpenEdge.Net.HTTP.StatusCodeEnum from propath.
using OpenEdge.Net.HTTP.IHttpResponse from propath.
using Progress.Json.ObjectModel.JsonConstruct from propath.
using Progress.Json.ObjectModel.JsonArray from propath.
using Progress.Json.ObjectModel.JsonObject from propath.
using com.sit.CommonMessageLogger from propath.
using com.sit.obp.web.OBPWebHandler from propath.
using com.sit.obp.web.OBPDefaultWebResponse from propath.
using com.sit.obp.ObjectFactory from propath.
using com.sit.obp.api.ClientResidencyWorkflow from propath.

block-level on error undo, throw.

class com.sit.obp.web.ClientResidenciesOBPWebHandler inherits OBPWebHandler:

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( poRequest as IWebRequest ):
        if poRequest:GetPathParameter( "ClientNumber" ) > "" then
            if poRequest:GetPathParameter( "EffectiveDate" ) > "" then
                return "GET,PATCH,DELETE".
            else
                return "GET,POST".

        /* Required path parameters are missing, throw a com.sit.exceptions.ArgumentException for the base class to handle. */
        undo, throw new com.sit.exceptions.ArgumentException().
    end method.

    /*------------------------------------------------------------------------------
    Purpose: Query client residency records
    Description:
    Notes:
    @param poRequest - the incoming web request
    @return Http status number
    ------------------------------------------------------------------------------*/
    method override protected integer HandleGet( input poRequest as IWebRequest ):
        define variable oHttpResponse       as IHttpResponse           no-undo.
        define variable oBody               as JsonConstruct           no-undo.
        define variable cRequiredParamValue as character               extent 1 no-undo.
        define variable cOptionalParamValue as character               extent 1 no-undo.
        define variable oWorkflow           as ClientResidencyWorkflow no-undo.
        define variable nClientNumber       as int64                   no-undo.
        define variable dtEffectiveDate     as date                    no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet" ).

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet", "ClientNumber,EffectiveDate", poRequest ).

         assign
            cRequiredParamValue[1] = substitute("ClientNumber,&1", poRequest:GetPathParameter( "ClientNumber" ) )
            cOptionalParamValue[1] = substitute("EffectiveDate,&1", poRequest:GetPathParameter( "EffectiveDate" ) )
            oHttpResponse          = OBPWebHandler:GetDefaultResponse()
            .

        do on error undo, throw:

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign
                nClientNumber   = int64( entry(2,cRequiredParamValue[1]) )
                dtEffectiveDate = this-object:ValidateAndConvertDateParam( "EffectiveDate", entry(2,cOptionalParamValue[1]))
                oWorkflow       = cast( ObjectFactory:New( get-class(ClientResidencyWorkflow) ), ClientResidencyWorkflow )
                .

           // If the given client number is less than 1, this will be an invalid request.
            if nClientNumber > 0 then do on error undo, throw:
               // If we have an effective date, then we call the specific endpoint, otherwise we will get the collection.
                if dtEffectiveDate <> ? then do:
                    oBody = oWorkflow:Query( nClientNumber, dtEffectiveDate ).
                    if valid-object( oBody ) then
                        oHttpResponse:Entity = oBody.
                    else
                        /* this was a specific request with no result, so we return a 404 */
                        oHttpResponse = OBPDefaultWebResponse:response404.
                end.
                else do:
                    oBody = oWorkflow:Query( nClientNumber ).
                    if valid-object( oBody ) then
                        oHttpResponse:Entity = oBody.
                    else
                        /* this was a collection request, so return an empty array */
                        oHttpResponse:Entity = new JsonArray().
                end.
            end.
            else
                oHttpResponse = OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        OBPWebHandler:WriteResponse( oHttpResponse ).
        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet" ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error:
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999", oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet", oErr ).
        end catch.
    end method.


    /*------------------------------------------------------------------------------
    Purpose: Create a new client residency record
    Description:
    Notes:
    @param poRequest - the incoming web request
    @return Http status number
    ------------------------------------------------------------------------------*/
    method override protected integer HandlePost( input poRequest as IWebRequest ):
        define variable oHttpResponse       as IHttpResponse           no-undo.
        define variable oRequestBody        as JsonObject              no-undo.
        define variable oResponse           as JsonObject              no-undo.
        define variable oWorkflow           as ClientResidencyWorkflow no-undo.
        define variable nClientNumber       as int64                   no-undo.
        define variable cRequiredParamValue as character               extent 1 no-undo.


        CommonMessageLogger:OperationStart( this-object:className, "HandlePost" ).

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePost", "ClientNumber", poRequest ).

        assign
            cRequiredParamValue[1] = substitute("ClientNumber,&1", poRequest:GetPathParameter( "ClientNumber" ) )
            oHttpResponse          = OBPWebHandler:GetDefaultResponse()
            .

        do on error undo, throw:

            this-object:ValidateParamValue(input cRequiredParamValue ).

            assign
                nClientNumber = int64( entry(2,cRequiredParamValue[1]) )
                oWorkflow     = cast( ObjectFactory:New( get-class(ClientResidencyWorkflow) ), ClientResidencyWorkflow )
                oRequestBody = cast( poRequest:Entity, JsonObject )
                .

           // If the given client number is less than 1, this will be an invalid request.
            if nClientNumber > 0 then do on error undo, throw:

                oResponse = oWorkflow:Create( nClientNumber, oRequestBody ).

                if valid-object( oResponse ) then
                    assign oHttpResponse:Entity = oResponse
                           oHttpResponse:StatusCode = integer( StatusCodeEnum:Created )
                    .
                else
                    oHttpResponse = OBPDefaultWebResponse:response404.
            end.
            else
                oHttpResponse = OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePost" ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.
    end method.

    /*------------------------------------------------------------------------------
    Purpose: Update a client residency record
    Description:
    Notes:
    @param poRequest - the incoming web request
    @return Http status number
    ------------------------------------------------------------------------------*/
    method override protected integer HandlePatch( input poRequest as IWebRequest ):
        define variable oHttpResponse       as IHttpResponse           no-undo.
        define variable oRequestBody        as JsonObject              no-undo.
        define variable oResponse           as JsonObject              no-undo.
        define variable oWorkflow           as ClientResidencyWorkflow no-undo.
        define variable nClientNumber       as int64                   no-undo.
        define variable cRequiredParamValue as character               extent 2 no-undo.
        define variable dtEffectiveDate     as date                    no-undo.


        CommonMessageLogger:OperationStart( this-object:className, "HandlePatch" ).

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePatch", "ClientNumber,EffectiveDate", poRequest ).

        assign
            cRequiredParamValue[1] = substitute("ClientNumber,&1", poRequest:GetPathParameter( "ClientNumber" ) )
            cRequiredParamValue[2] = substitute("EffectiveDate,&1", poRequest:GetPathParameter( "EffectiveDate" ) )
            oHttpResponse          = OBPWebHandler:GetDefaultResponse()
            .

        do on error undo, throw:

            this-object:ValidateParamValue(input cRequiredParamValue ).

            assign
                nClientNumber = int64( entry(2,cRequiredParamValue[1]) )
                oWorkflow     = cast( ObjectFactory:New( get-class(ClientResidencyWorkflow) ), ClientResidencyWorkflow )
                dtEffectiveDate = this-object:ValidateAndConvertDateParam( "EffectiveDate", entry(2,cRequiredParamValue[2]))
                oRequestBody = cast( poRequest:Entity, JsonObject )
                .

           // If the given client number is less than 1, or we didn't get an effective date, this will be an invalid request.
            if nClientNumber > 0 and dtEffectiveDate <> ? then do on error undo, throw:

                oResponse = oWorkflow:Update( nClientNumber, dtEffectiveDate, oRequestBody ).

                if valid-object( oResponse ) then
                    oHttpResponse:Entity = oResponse.
                else
                    oHttpResponse = OBPDefaultWebResponse:response404.
            end.
            else
                oHttpResponse = OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePatch" ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.
    end method.

    /*------------------------------------------------------------------------------
    Purpose: delete a client residency record
    Description:
    Notes:
    @param poRequest - the incoming web request
    @return Http status number
    ------------------------------------------------------------------------------*/
    method override protected integer HandleDelete( input poRequest as IWebRequest ):
        define variable oHttpResponse       as IHttpResponse           no-undo.
        define variable oWorkflow           as ClientResidencyWorkflow no-undo.
        define variable nClientNumber       as int64                   no-undo.
        define variable cRequiredParamValue as character               extent 2 no-undo.
        define variable dtEffectiveDate     as date                    no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleDelete" ).

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleDelete", "ClientNumber,EffectiveDate", poRequest ).

        assign
            cRequiredParamValue[1] = substitute("ClientNumber,&1", poRequest:GetPathParameter( "ClientNumber" ) )
            cRequiredParamValue[2] = substitute("EffectiveDate,&1", poRequest:GetPathParameter( "EffectiveDate" ) )
            oHttpResponse          = OBPWebHandler:GetDefaultResponse()
            .

        do on error undo, throw:

            this-object:ValidateParamValue(input cRequiredParamValue ).

            assign
                nClientNumber = int64( entry(2,cRequiredParamValue[1]) )
                oWorkflow     = cast( ObjectFactory:New( get-class(ClientResidencyWorkflow) ), ClientResidencyWorkflow )
                dtEffectiveDate = this-object:ValidateAndConvertDateParam( "EffectiveDate", entry(2,cRequiredParamValue[2]))
                .

           // If the given client number is less than 1, or we didn't get an effective date, this will be an invalid request.
            if nClientNumber > 0 and dtEffectiveDate <> ? then do on error undo, throw:
                oWorkflow:Delete( nClientNumber, dtEffectiveDate ).
                oHttpResponse:StatusCode = integer( StatusCodeEnum:NoContent ).
            end.
            else
                oHttpResponse = OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleDelete" ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.
    end method.
end class.
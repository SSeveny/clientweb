 
 /*------------------------------------------------------------------------
    File        : ClientCommunicationModesOBPWebHandler
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : chrisr
    Created     : Aug 29, 2018
    Notes       : 
  ----------------------------------------------------------------------*/

using Progress.Lang.*.
using OpenEdge.Web.*.
using OpenEdge.Net.HTTP.StatusCodeEnum.
using Progress.Json.ObjectModel.*.
using com.sit.CommonMessageLogger.
using com.sit.obp.web.*.
using com.sit.obp.api.ClientCommunicationModesWorkflow.
using com.sit.obp.ObjectFactory.

block-level on error undo, throw.

class com.sit.obp.web.ClientCommunicationModesOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( input poRequest as IWebRequest ):
            if length(poRequest:GetPathParameter( "ClientNumber":U ),"Character") > 0 then
                return "GET,PATCH":U.

            /* If there is a problem with the incoming request, throw an ArgumentException for the base class to catch and deal with */
             undo, throw new com.sit.exceptions.ArgumentException().
    end method.

    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oBody as JsonConstruct no-undo.
        define variable dClientNumberPathParameter as decimal no-undo.
        define variable oWorkflow as ClientCommunicationModesWorkflow no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()               
               oWorkflow = cast( ObjectFactory:New( get-class( ClientCommunicationModesWorkflow ) ), ClientCommunicationModesWorkflow )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet":U, "ClientNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            dClientNumberPathParameter = decimal(entry(2,cRequiredParamValue[1])).
             
            /* Example of setting up the request, and returning an HTTP 400 if required information is not present */
            if dClientNumberPathParameter > 0 then 
            do:
                oBody = oWorkflow:Query( dClientNumberPathParameter ).
                if valid-object(oBody) then
                    oHttpResponse:Entity = oBody.
                else
                    oHttpResponse:Entity = new JsonArray().
            end.
            /* If no CIFNumber parameter is present, this is a bad request */
            else
                oHttpResponse = OBPDefaultWebResponse:response400.
            
            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet":U, oErr ).
        end catch.
    end method.

    method override protected integer HandlePatch( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oBody as JsonConstruct no-undo.
        define variable dClientNumberPathParameter as decimal no-undo.
        define variable oWorkflow as ClientCommunicationModesWorkflow no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePatch":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()               
               oWorkflow = cast( ObjectFactory:New( get-class( ClientCommunicationModesWorkflow ) ), ClientCommunicationModesWorkflow )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePatch":U, "ClientNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            dClientNumberPathParameter = decimal(entry(2,cRequiredParamValue[1])).
            
            /* Example of setting up the request, and returning an HTTP 400 if required information is not present */
            if dClientNumberPathParameter > 0 then  
            do: 
                assign oBody = oWorkflow:Update( dClientNumberPathParameter, cast( poRequest:Entity, JsonArray ) )

                       oHttpResponse:Entity = oBody
                .
            /* If no CIFNumber parameter is present, this is a bad request */
            end.
            else
                oHttpResponse = OBPDefaultWebResponse:response400.
            
            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePatch":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePatch":U, oErr ).
        end catch.
    end method.

end class.
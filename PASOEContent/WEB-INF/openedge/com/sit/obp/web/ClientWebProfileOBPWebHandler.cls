
/*------------------------------------------------------------------------
   File        : ClientWebProfileOBPWebHandler
   Purpose     :
   Syntax      :
   Description :
   Author(s)   : bennettb
   Created     : Thu Feb 28 09:58:24 EST 2019
   Notes       :
 ----------------------------------------------------------------------*/

using Progress.Lang.*.
using com.sit.obp.web.OBPWebHandler from propath.
using com.sit.obp.api.ClientWebProfileWorkflow from propath.
using com.sit.CommonMessageLogger from propath.
using com.sit.obp.ObjectFactory from propath.

block-level on error undo, throw.

class com.sit.obp.web.ClientWebProfileOBPWebHandler inherits OBPWebHandler:

    method public override character SupportedHttpMethods( input poRequest as OpenEdge.Web.IWebRequest):
        return "GET":U.
    end method.

    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse             as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oClientWebProfileWorkflow as ClientWebProfileWorkflow        no-undo.
        define variable cRequiredParamValue       as character                       extent 1 no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U ).

        assign
            oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet":U, "GUID":U, poRequest ).

        do on error undo, throw:
            assign
                cRequiredParamValue[1] = substitute("GUID,&1":U,poRequest:GetPathParameter( "GUID":U )).

            this-object:ValidateParamValue(input cRequiredParamValue).

            oClientWebProfileWorkflow = cast( ObjectFactory:New(get-class (ClientWebProfileWorkflow)),
                ClientWebProfileWorkflow ).

            oHttpResponse:Entity = oClientWebProfileWorkflow:QueryWebProfile(poRequest:GetPathParameter( "GUID":U )).

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet":U, oErr ).
        end catch.
    end method.

end class.

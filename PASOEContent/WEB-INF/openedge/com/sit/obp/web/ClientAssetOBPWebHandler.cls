 /*------------------------------------------------------------------------
    File        : ClientAssetOBPWebHandler
    Purpose     : Called by PAS to handle client asset reqeust for CRUD operations
    Syntax      :
    Description :
    Author(s)   : danielc
    Created     : July 20, 2018
    Notes       :
  ----------------------------------------------------------------------*/

using Progress.Lang.*.
using OpenEdge.Web.*.
using OpenEdge.Net.HTTP.StatusCodeEnum.
using Progress.Json.ObjectModel.*.
using com.sit.serializer.*.
using com.sit.obp.api.ClientAssetLiabilityWorkflow.
using com.sit.obp.exceptions.*.
using com.sit.obp.ObjectFactory.
using com.sit.CommonMessageLogger.

block-level on error undo, throw.

class com.sit.obp.web.ClientAssetOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

    define private property Site as character no-undo
       get():
            if this-object:Site = "":U then
                this-object:Site = dynamic-function( "getSessionProperty":U, "SiteCode":U ).

            return this-object:Site.
        end get.
        set.

    define private property Company as character no-undo
       get():
            if this-object:Company = "":U then
                this-object:Company = dynamic-function( "getSessionProperty":U, "CompanyCode":U ).

            return this-object:Company.
        end get.
        set.

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( input poRequest as IWebRequest ):
        if length( poRequest:GetPathParameter( "SequenceNumber":U ),"Character":U ) > 0 then
            return "GET,PUT,DELETE":U.
        else
            return "GET,POST":U.

        /* Required path parameters are missing, throw a com.sit.exceptions.ArgumentException for the base class to handle. */
        undo, throw new com.sit.exceptions.ArgumentException().
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle query request for client asset
    Description:
    Notes:
    @param poRequest
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oAssetObject as JsonObject no-undo.
        define variable oAssetArray as JsonArray no-undo.
        define variable nClientNumber as integer no-undo.
        define variable nSequence as integer no-undo.
        define variable oAssetLiabilityWorkflow as ClientAssetLiabilityWorkflow no-undo.
        define variable oParamList as Progress.Lang.ParameterList no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent 1 no-undo.
        define variable cInternalOrExternal as character no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U ).

        oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet":U, "ClientNumber,SequenceNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cOptionalParamValue[1] = substitute("SequenceNumber,&1":U, poRequest:GetPathParameter( "SequenceNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber    = integer(entry(2,cRequiredParamValue[1]))
                   nSequence  = integer(entry(2,cOptionalParamValue[1])).

            if nClientNumber > 0 then
            do:
                oParamList = new Progress.Lang.ParameterList(3).
                oParamList:SetParameter(1, "Character":U, "input":U, this-object:Company).
                oParamList:SetParameter(2, "Character":U, "input":U, this-object:Site).
                oParamList:SetParameter(3, "Logical":U, "input":U, true).

                oAssetLiabilityWorkflow =
                    cast( ObjectFactory:New( "com.sit.obp.api.ClientAssetLiabilityWorkflow":U, oParamList ),
                                             com.sit.obp.api.ClientAssetLiabilityWorkflow ).
                
                // GetPathParameter returns empty string when parameter is missing
                if entry(2,cOptionalParamValue[1]) > "":U then
                do:
                    /* If we've been provided a sequence number, then this is a request for a specific, external asset */
                    oAssetArray = oAssetLiabilityWorkflow:QueryExternalAssets( input nClientNumber, input nSequence ).

                    if valid-object(oAssetArray) and oAssetArray:length = 1 then
                        oHttpResponse:Entity = oAssetArray:GetJsonObject(1).
                    else
                        oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
                end.
                else do:
                    cInternalOrExternal = com.sit.obp.web.OBPWebHandler:GetQueryParamValue( "InternalOrExternal":U, poRequest ).

                    /* For the URL where no sequence number is supplied, there is also a query parameter that allows requests for
                       only internal or external accounts. Check if that is set, and decide which workflow method to use */
                    if cInternalOrExternal = "int":U then do:
                        assign oAssetObject = new JsonObject()
                               oAssetArray = oAssetLiabilityWorkflow:QueryInternalAssets( input nClientNumber )
                        .
                        
                        if valid-object( oAssetArray ) then
                            oAssetObject:Add( "Internal":U, oAssetArray ).
                    end.
                    else if cInternalOrExternal = "ext":U then do:
                        assign oAssetObject = new JsonObject()
                               oAssetArray = oAssetLiabilityWorkflow:QueryExternalAssets( input nClientNumber, input -1 )
                        .
                        
                        if valid-object( oAssetArray ) then
                            oAssetObject:Add( "External":U, oAssetArray ).
                    end.
                    else
                        /* Get all client's assets */
                        oAssetObject = oAssetLiabilityWorkflow:QueryAssets( input nClientNumber ).

                    if valid-object( oAssetObject ) then
                        oHttpResponse:Entity = oAssetObject.
                    else
                        oHttpResponse:Entity = new JsonObject().
                end.
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet":U, oErr ).
        end catch.
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle create request for client asset
    Description:
    Notes:
    @param poRequest Json object contains client asset information to be created by the workflow
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandlePost( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson as JsonObject no-undo.
        define variable nClientNumber as integer no-undo.
        define variable oAssetLiabilityWorkflow as ClientAssetLiabilityWorkflow no-undo.
        define variable oParamList as Progress.Lang.ParameterList no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePost":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()
               oJson = cast( poRequest:Entity, JsonObject ).

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePost":U, "ClientNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            nClientNumber = integer(entry(2,cRequiredParamValue[1])).

            if nClientNumber > 0 then
            do:
                oParamList = new Progress.Lang.ParameterList(3).
                oParamList:SetParameter(1, "Character":U, "input":U, this-object:Company).
                oParamList:SetParameter(2, "Character":U, "input":U, this-object:Site).
                oParamList:SetParameter(3, "Logical":U, "input":U, true).

                oAssetLiabilityWorkflow =
                    cast( ObjectFactory:New( "com.sit.obp.api.ClientAssetLiabilityWorkflow":U, oParamList ),
                                             com.sit.obp.api.ClientAssetLiabilityWorkflow ).

                oJson = oAssetLiabilityWorkflow:CreateAsset(input nClientNumber, input oJson).

                if valid-object(oJson) then
                    assign oHttpResponse:Entity = oJson
                           oHttpResponse:StatusCode = integer( StatusCodeEnum:Created ).
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePost":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePost":U, oErr ).
        end catch.
    end method.


   /*------------------------------------------------------------------------
    Purpose:  Handle replace request for client asset
    Description:
    Notes:
    @param poRequest Json object contains asset information to replace specified asset by the workflow
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandlePut( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson as JsonObject no-undo.
        define variable nClientNumber as integer no-undo.
        define variable nSequence as integer no-undo.
        define variable oAssetLiabilityWorkflow as ClientAssetLiabilityWorkflow no-undo.
        define variable oParamList as Progress.Lang.ParameterList no-undo.
        define variable cRequiredParamValue as character extent 2 no-undo.
        define variable cOptionalParamValue as character extent no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePut":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePut":U, "ClientNumber,SequenceNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("SequenceNumber,&1":U,poRequest:GetPathParameter( "SequenceNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer( entry(2,cRequiredParamValue[1]) )
                   nSequence = integer( entry(2,cRequiredParamValue[2]) ).

            if nClientNumber > 0 then
            do:
                oParamList = new Progress.Lang.ParameterList(3).
                oParamList:SetParameter(1, "Character":U, "input":U, this-object:Company).
                oParamList:SetParameter(2, "Character":U, "input":U, this-object:Site).
                oParamList:SetParameter(3, "Logical":U, "input":U, true).

                oAssetLiabilityWorkflow =
                    cast( ObjectFactory:New( "com.sit.obp.api.ClientAssetLiabilityWorkflow":U, oParamList ),
                                             com.sit.obp.api.ClientAssetLiabilityWorkflow ).

                oJson = oAssetLiabilityWorkflow:ReplaceAsset(input nClientNumber,
                                                             input nSequence,
                                                             input cast( poRequest:Entity, JsonObject )).

                if valid-object(oJson) then
                    /* We return a 204 here because the data provided to this method has successfully overwritten the record.
                       This means that client application already has the current version of it, we don't need to send it back.
                       Using HTTP 204 tells the client that this is the case. */
                    oHttpResponse:StatusCode = integer( StatusCodeEnum:NoContent ).
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePut":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePut":U, oErr ).
        end catch.
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle delete request for client asset
    Description:
    Notes:
    @param poRequest Json object contains client asset to be deleted by the workflow
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandleDelete( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable nClientNumber as integer no-undo.
        define variable nSequence as integer no-undo.
        define variable oAssetLiabilityWorkflow as ClientAssetLiabilityWorkflow no-undo.
        define variable oParamList as Progress.Lang.ParameterList no-undo.
        define variable cRequiredParamValue as character extent 2 no-undo.
        define variable cOptionalParamValue as character extent no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleDelete":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleDelete":U, "ClientNumber,SequenceNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("SequenceNumber,&1":U,poRequest:GetPathParameter( "SequenceNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer( entry(2,cRequiredParamValue[1]) )
                   nSequence = integer( entry(2,cRequiredParamValue[2]) ).

            if nClientNumber > 0 then
            do:
                oParamList = new Progress.Lang.ParameterList(3).
                oParamList:SetParameter(1, "Character":U, "input":U, this-object:Company).
                oParamList:SetParameter(2, "Character":U, "input":U, this-object:Site).
                oParamList:SetParameter(3, "Logical":U, "input":U, true).

                oAssetLiabilityWorkflow =
                    cast( ObjectFactory:New( "com.sit.obp.api.ClientAssetLiabilityWorkflow":U, oParamList ),
                                             com.sit.obp.api.ClientAssetLiabilityWorkflow ).

                oAssetLiabilityWorkflow:DeleteAsset( input nClientNumber,
                                                     input nSequence).

                /* We return a 204 here because the request provided has caused the successful deletion of the record.
                   This means that there's nothing to return to the client application.
                   Using HTTP 204 tells the client that this is the case. */
                oHttpResponse:StatusCode = integer( StatusCodeEnum:NoContent ).
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationStart( this-object:className, "HandleDelete":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleDelete":U, oErr ).
        end catch.
    end method.

end class.
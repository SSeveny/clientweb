
 /*------------------------------------------------------------------------
    File        : ClientMarketingOBPWebHandler
    Purpose     : Called by PAS to handle client marketing CRUD operations
    Syntax      :
    Description :
    Author(s)   : lauries
    Created     : Tue Aug 21 11:55:01 EDT 2018
    Notes       :
  ----------------------------------------------------------------------*/

using Progress.Lang.*.
using OpenEdge.Web.*.
using OpenEdge.Net.HTTP.StatusCodeEnum.
using Progress.Json.ObjectModel.*.
using com.sit.obp.exceptions.*.
using com.sit.obp.ObjectFactory.
using com.sit.CommonMessageLogger.
using com.sit.obp.api.ClientMarketingWorkflowBase.

block-level on error undo, throw.

class com.sit.obp.web.ClientMarketingOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

   define private property Site as character no-undo
       get():
            if this-object:Site = "":U then
                this-object:Site = dynamic-function( "getSessionProperty":U, "SiteCode":U ).

            return this-object:Site.
        end get.
        set.

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( poRequest as IWebRequest ):
        if length(poRequest:GetPathParameter( "ClientNumber":U ), "Character":U) > 0 then
            return "GET,PUT":U.

        /* Required path parameters are missing, throw a com.sit.exceptions.ArgumentException for the base class to handle. */
        undo, throw new com.sit.exceptions.ArgumentException().
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle query request for client's marketing configuration
    Description:
    Notes:
    @param poRequest
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson as JsonObject no-undo.
        define variable nClientNumber as integer no-undo.
        define variable oClientMarketingWorkflow as ClientMarketingWorkflowBase no-undo.
        define variable oParamList as Progress.Lang.ParameterList no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet":U, "ClientNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer(entry(2,cRequiredParamValue[1])).

            if nClientNumber > 0 then
            do:
                oParamList = new Progress.Lang.ParameterList(1).
                oParamList:SetParameter(1, "Character":U, "input":U, this-object:Site).

                oClientMarketingWorkflow = cast( ObjectFactory:New( "com.sit.obp.api.ClientMarketingWorkflow":U, oParamList),
                                             com.sit.obp.api.ClientMarketingWorkflowBase ).

                oJson = oClientMarketingWorkflow:Query(input nClientNumber).

                if valid-object(oJson) then
                    assign oHttpResponse:Entity = oJson.
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet":U, oErr ).
        end catch.
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle replace request for client's marketing configuration
    Description:
    Notes:
    @param poRequest Json object contains client marketing information
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandlePut( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson as JsonObject no-undo.
        define variable nClientNumber as integer no-undo.
        define variable oClientMarketingWorkflow as ClientMarketingWorkflowBase no-undo.
        define variable oParamList as Progress.Lang.ParameterList no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePut":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePut":U, "ClientNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer(entry(2,cRequiredParamValue[1])).

            if nClientNumber > 0 then
            do:
                oParamList = new Progress.Lang.ParameterList(1).
                oParamList:SetParameter(1, "Character":U, "input":U, this-object:Site).

                oClientMarketingWorkflow = cast( ObjectFactory:New( "com.sit.obp.api.ClientMarketingWorkflow":U, oParamList),
                                             com.sit.obp.api.ClientMarketingWorkflowBase ).

                oJson = oClientMarketingWorkflow:Replace(input nClientNumber,input cast( poRequest:Entity, JsonObject )).

                if valid-object(oJson) then
                    /* We return a 204 here because the data provided to this method has successfully overwritten the record.
                       This means that client application already has the current version of it, we don't need to send it back.
                       Using HTTP 204 tells the client that this is the case. */
                    oHttpResponse:StatusCode = integer( StatusCodeEnum:NoContent ).
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePut":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePut":U, oErr ).
        end catch.
    end method.
end class.
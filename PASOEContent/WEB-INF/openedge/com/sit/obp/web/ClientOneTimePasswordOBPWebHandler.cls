
 /*------------------------------------------------------------------------
    File        : ClientOneTimePasswordOBPWebHandler
    Purpose     :
    Syntax      :
    Description :
    Author(s)   : bennettb
    Created     : Tue Oct 09 10:34:46 EDT 2018
    Notes       :
  ----------------------------------------------------------------------*/

using OpenEdge.Web.WebResponseWriter from propath.
using OpenEdge.Web.IWebRequest from propath.
using OpenEdge.Net.HTTP.StatusCodeEnum from propath.
using Progress.Json.ObjectModel.JsonObject from propath.
using com.sit.obp.api.ClientOneTimePasswordWorkflowAbstract from propath.
using com.sit.obp.ObjectFactory from propath.
using com.sit.exceptions.ArgumentException from propath.
using com.sit.CommonMessageLogger from propath.
using com.sit.obp.exceptions.BusinessRuleException from propath.
using com.sit.obp.web.OBPWebHandler from propath.
using com.sit.obp.config.ApplicationConfiguration from propath.

block-level on error undo, throw.

class com.sit.obp.web.ClientOneTimePasswordOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( input poRequest as IWebRequest ):
        if length(poRequest:GetPathParameter( "ClientNumber":U ), "Character":U) > 0 then
            return "GET,POST":U.

        undo, throw new ArgumentException().
    end method.

    /*------------------------------------------------------------------------
    Purpose:  Handle OTP Generation for Client Asset
    Description:
    Notes:
    @param poRequest Json object contains client asset information to be created by the workflow
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable nClientNumber as integer no-undo.
        define variable cClientNumber as character no-undo.
        define variable cMessage as character no-undo.
        define variable oBody as JsonObject no-undo.
        define variable oOTPWorkflow as ClientOneTimePasswordWorkflowAbstract no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.
        define variable cDebugMode as character no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse(StatusCodeEnum:NoContent)
               oOTPWorkflow = cast(ObjectFactory:New("com.sit.obp.api.ClientOneTimePasswordWorkflow":U), ClientOneTimePasswordWorkflowAbstract)
               cDebugMode = ApplicationConfiguration:KeyValueConfiguration:GetValue( "DebugMode":U )
               oOTPWorkflow:DebugMode = ( cDebugMode <> ? and cDebugMode = "ON":U )
               
               cClientNumber = poRequest:GetPathParameter( "ClientNumber":U )
               nClientNumber = integer( cClientNumber )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet":U, "ClientNumber":U, poRequest ).

        do on error undo, throw: 
            assign cRequiredParamValue[1] = substitute( "ClientNumber,&1":U, cClientNumber ).
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            if nClientNumber > 0 then
            do:
                cMessage = oOTPWorkflow:SendPassword(nClientNumber).
    
                if cMessage <> ? then do:
                    oBody = new JsonObject().
                    oBody:Add("ResponseMessage":U, cMessage).
                    oHttpResponse = OBPWebHandler:GetDefaultResponse().
                    oHttpResponse:Entity = oBody.
                end.
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
            
            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch. 
        end.
        
        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).
        
        this-object:setHttpResponse(oHttpResponse).
        
        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet":U, oErr ).
        end catch.
    end method.

    /*------------------------------------------------------------------------
    Purpose:  Handle OTP Validation for Client Asset
    Description:
    Notes:
    @param poRequest Json object contains client asset information to be created by the workflow
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandlePost( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson as JsonObject no-undo.
        define variable nClientNumber as integer no-undo.
        define variable oOTPWorkflow as ClientOneTimePasswordWorkflowAbstract no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent   no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePost":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse(StatusCodeEnum:NoContent).

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePost":U, "ClientNumber":U, poRequest ).

        do on error undo, throw: 
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            assign nClientNumber = integer(entry(2,cRequiredParamValue[1])).
            
            if nClientNumber > 0 then
            do:
                oOTPWorkflow = cast(ObjectFactory:New("com.sit.obp.api.ClientOneTimePasswordWorkflow":U), ClientOneTimePasswordWorkflowAbstract).
                
                oJson = cast(poRequest:Entity, JsonObject).
    
                if not oOTPWorkflow:ValidatePassword(nClientNumber, oJson:GetCharacter("Password":U)) then
                    undo, throw new BusinessRuleException("Invalid Password":U).
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.
        
            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch. 
        end.
        
        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).
        
        this-object:setHttpResponse(oHttpResponse).
        
        CommonMessageLogger:OperationEnd( this-object:className, "HandlePost":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePost":U, oErr ).
        end catch.
    end method.

end class.

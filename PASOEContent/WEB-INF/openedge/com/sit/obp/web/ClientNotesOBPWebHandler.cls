 /*------------------------------------------------------------------------
    File        : ClientNotesOBPWebHandler
    Purpose     : Called by PAS to handle client's note reqeust for create and read operations
    Syntax      :
    Description :
    Author(s)   : danielc
    Created     : May 28, 2018
    Notes       :
  ----------------------------------------------------------------------*/

using Progress.Lang.*.
using OpenEdge.Web.*.
using OpenEdge.Net.HTTP.StatusCodeEnum.
using Progress.Json.ObjectModel.*.
using com.sit.serializer.*.
using com.sit.obp.api.ClientNoteWorkflow.
using com.sit.obp.exceptions.*.
using com.sit.obp.ObjectFactory.
using com.sit.CommonMessageLogger.

block-level on error undo, throw.

class com.sit.obp.web.ClientNotesOBPWebHandler inherits com.sit.obp.web.OBPWebHandler:

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( poRequest as IWebRequest ):
        if length(poRequest:GetPathParameter( "ClientNumber":U ), "Character":U) > 0 then
            return "GET,POST":U.

        /* Required path parameters are missing, throw a com.sit.exceptions.ArgumentException for the base class to handle. */
        undo, throw new com.sit.exceptions.ArgumentException().
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle query request for client's notes
    Description:
    Notes:
    @param poRequest
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson as JsonObject no-undo.
        define variable nClientNumber as integer no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent no-undo.
        define variable oClientNoteWorkflow as ClientNoteWorkflow no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer(entry(2,cRequiredParamValue[1])).

            if nClientNumber > 0 then
            do:
                oClientNoteWorkflow = cast( ObjectFactory:New( "com.sit.obp.api.ClientNoteWorkflow":U), com.sit.obp.api.ClientNoteWorkflow ).

                oJson = oClientNoteWorkflow:Query(input nClientNumber).

                if valid-object(oJson) then
                    assign oHttpResponse:Entity = oJson.
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
            end.
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            this-object:agentLogger:Error( new com.sit.exceptions.AppError( oErr ) ).
        end catch.
    end method.

   /*------------------------------------------------------------------------
    Purpose:  Handle create request for client's note
    Description:
    Notes:
    @param poRequest Json object contains note to be created by the workflow
    @return Http status number
    ------------------------------------------------------------------------*/
    method override protected integer HandlePost( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oJson as JsonObject no-undo.
        define variable nClientNumber as integer no-undo.
        define variable nMaximumLength as integer initial 31991 no-undo. /* maximum size character data type can hold */
        define variable cNote as longchar no-undo.
        define variable cLogText as longchar initial "ClientNumber=&1;Note=&2":U no-undo.
        define variable cRequiredParamValue as character extent 2 no-undo.
        define variable cOptionalParamValue as character extent no-undo.
        define variable oClientNoteWorkflow as ClientNoteWorkflow no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePost":U ).

        oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePost":U, "ClientNumber":U, poRequest ).

        do on error undo, throw:

            /* The specification is for the note to use text/plain mime type, which gets transformed into an OpenEdge.Core.Memptr */
            cNote = cast( poRequest:Entity, JsonObject ):GetLongchar( "Note":U ).

            if length(cNote, "raw":U) > nMaximumLength then  /*length(cNote, "longchar") is not valid, so have to use "raw" here*/
                undo, throw new com.sit.obp.exceptions.BusinessRuleException( substitute("Parameter Length [&1] Exceeded the Maximum Size Limit [&2]":T, length(cNote, "raw"), nMaximumLength)).

            cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).

            do on error undo, throw:
               cRequiredParamValue[2] = ("Note,":U + cNote).
               catch oError as Progress.Lang.SysError :
                  if oError:GetMessageNum( 1 ) = integer( com.sit.errorstatus.ErrorStatusEnum:ExceedMaximumCharSize ) or
                     oError:GetMessageNum( 1 ) = integer( com.sit.errorstatus.ErrorStatusEnum:ReplaceConcatExceedMaxCharSize ) or
                     oError:GetMessageNum( 1 ) = integer( com.sit.errorstatus.ErrorStatusEnum:UpdateExceedingMaxCharSize ) then
                      undo, throw new com.sit.obp.exceptions.BusinessRuleException( "Note Length Exceeded the Maximum Size Limit" ).
                  undo, throw oError.
               end catch.
            end.

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer(entry(2,cRequiredParamValue[1])).

            if nClientNumber > 0 and cNote > "":U then
            do:
                oClientNoteWorkflow = cast( ObjectFactory:New( "com.sit.obp.api.ClientNoteWorkflow":U), com.sit.obp.api.ClientNoteWorkflow ).

                oJson = oClientNoteWorkflow:Create( input nClientNumber, input cNote ).

                if valid-object(oJson) and oJson:Has("Notes":U) then
                    assign oHttpResponse:Entity = oJson
                           oHttpResponse:StatusCode = integer( StatusCodeEnum:Created ).
                else
                    oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response404.
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = com.sit.obp.web.OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.

        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePost":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePost":U, oErr ).
        end catch.
    end method.

end class.
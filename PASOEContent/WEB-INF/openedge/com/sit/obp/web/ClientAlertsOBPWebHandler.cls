
 /*------------------------------------------------------------------------
    File        : ClientAlertsOBPWebHandler
    Purpose     :
    Syntax      :
    Description :
    Author(s)   : chrisr
    Created     : Aug 23, 2018 10:59:50 AM
    Notes       :
  ----------------------------------------------------------------------*/

block-level on error undo, throw.

using OpenEdge.Net.HTTP.StatusCodeEnum from propath.
using OpenEdge.Web.IWebRequest from propath.
using Progress.Json.ObjectModel.JsonConstruct from propath.
using Progress.Json.ObjectModel.JsonObject from propath.
using com.sit.CommonMessageLogger from propath.
using com.sit.obp.ObjectFactory from propath.
using com.sit.obp.api.ClientAlertsWorkflow from propath.
using com.sit.obp.model.ClientAlertModel from propath.
using com.sit.obp.web.OBPDefaultWebResponse from propath.
using com.sit.obp.web.OBPWebHandler from propath.
using com.sit.obp.config.ApplicationConfiguration from propath.

class com.sit.obp.web.ClientAlertsOBPWebHandler inherits OBPWebHandler:

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( input poRequest as IWebRequest ):
        if length(poRequest:GetPathParameter( "ClientNumber":U ),"Character":U) > 0 then
            if length( poRequest:GetPathParameter( "Company":U ), "Character":U ) > 0 then
                return "GET,PATCH":U.
            else
                return "POST":U.

        undo, throw new com.sit.exceptions.ArgumentException().
    end method.

    method override protected integer HandleGet( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable nClientNumberPathParameter as integer no-undo.
        define variable cCompanyPathParameter as character no-undo.
        define variable oAlertWorkflow as ClientAlertsWorkflow no-undo.
        define variable oBody as JsonObject no-undo.
        define variable cRequiredParamValue as character extent 2 no-undo.
        define variable cOptionalParamValue as character extent no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()
               cCompanyPathParameter = poRequest:GetPathParameter( "Company":U )

               /* Instantiate a new workflow object for every method invocation because the web handlers are cached */
               oAlertWorkflow = cast( ObjectFactory:New( get-class( ClientAlertsWorkflow ) ), ClientAlertsWorkflow )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet":U, "ClientNumber,Company":U, poRequest ).

        do on error undo, throw: 
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute("Company,&1":U, cCompanyPathParameter).
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            nClientNumberPathParameter = integer(entry(2,cRequiredParamValue[1])).
            
            if nClientNumberPathParameter > 0 then
            do:
                oBody = oAlertWorkflow:Query( nClientNumberPathParameter, cCompanyPathParameter ).
    
                if valid-object( oBody ) then
                    oHttpResponse:Entity = oBody.
                else
                    oHttpResponse = OBPDefaultWebResponse:response404.
            end.
            else
              oHttpResponse = OBPDefaultWebResponse:response400.
              
            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch. 
        end.
        
        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet":U, oErr ).
        end catch.
    end method.

    method override protected integer HandlePatch( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable nClientNumberPathParameter as integer no-undo.
        define variable cCompanyPathParameter as character no-undo.
        define variable oRequestBody as JsonObject no-undo.
        define variable oAlertWorkflow as ClientAlertsWorkflow no-undo.
        define variable cRequiredParamValue as character extent 2 no-undo.
        define variable cOptionalParamValue as character extent no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePatch":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse()
               oRequestBody = cast( poRequest:Entity, JsonObject )
               cCompanyPathParameter = poRequest:GetPathParameter( "Company":U )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePatch":U, "ClientNumber,Company":U, poRequest ).

        do on error undo, throw: 
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U ))
                   cRequiredParamValue[2] = substitute( "Company,&1":U, cCompanyPathParameter )
            .
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            nClientNumberPathParameter = integer(entry(2,cRequiredParamValue[1])).
            
            if nClientNumberPathParameter > 0 then
            do:
                /* Instantiate a new workflow object for every method invocation because the web handlers are cached */
                oAlertWorkflow = cast( ObjectFactory:New( get-class( ClientAlertsWorkflow ) ), ClientAlertsWorkflow ).
                oHttpResponse:Entity = oAlertWorkflow:Update( nClientNumberPathParameter, cCompanyPathParameter, oRequestBody ).
            end.
            else
              oHttpResponse = OBPDefaultWebResponse:response400.
        
            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch. 
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePatch":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePatch":U, oErr ).
        end catch.
    end method.

    method override protected integer HandlePost( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse as OpenEdge.Net.HTTP.IHttpResponse no-undo.
        define variable oBody as JsonObject no-undo.
        define variable nClientNumberPathParameter as integer no-undo.
        define variable oRequestBody as JsonObject no-undo.
        define variable oAlertWorkflow as ClientAlertsWorkflow no-undo.
        define variable cAlertMessage as character no-undo.
        define variable cRequiredParamValue as character extent 1 no-undo.
        define variable cOptionalParamValue as character extent no-undo.
        define variable cDebugMode as character no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePost":U ).

        assign oHttpResponse = com.sit.obp.web.OBPWebHandler:GetDefaultResponse(StatusCodeEnum:NoContent)
               oRequestBody = cast( poRequest:Entity, JsonObject )

               /* Instantiate a new workflow object for every method invocation because the web handlers are cached */
               oAlertWorkflow = cast( ObjectFactory:New( get-class( ClientAlertsWorkflow ) ), ClientAlertsWorkflow )
               cDebugMode = ApplicationConfiguration:KeyValueConfiguration:GetValue( "DebugMode":U )
               oAlertWorkflow:DebugMode = ( cDebugMode <> ? and cDebugMode = "ON":U )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePost":U, "ClientNumber":U, poRequest ).

        do on error undo, throw: 
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber":U )).
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            nClientNumberPathParameter = integer(entry(2,cRequiredParamValue[1])).
            
            if nClientNumberPathParameter > 0 then
            do:
                cAlertMessage = oAlertWorkflow:TriggerClientAlert( nClientNumberPathParameter, oRequestBody ).
    
                if cAlertMessage <> ? then do:
                    oBody = new JsonObject().
                    oBody:Add("ResponseMessage":U, cAlertMessage).
                    oHttpResponse = OBPWebHandler:GetDefaultResponse().
                    oHttpResponse:Entity = oBody.
                end.
            end.
            else
              oHttpResponse = OBPDefaultWebResponse:response400.
        
            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch. 
        end.

        com.sit.obp.web.OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePost":U ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePost":U, oErr ).
        end catch.
    end method.
end class.

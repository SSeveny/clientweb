 /*------------------------------------------------------------------------
    File        : ClientNextOfKinOBPWebHandler
    Purpose     :
    Syntax      :
    Description :
    Author(s)   :
    Created     : May 16, 2018
    Notes       :
  ----------------------------------------------------------------------*/

block-level on error undo, throw.

using OpenEdge.Net.HTTP.StatusCodeEnum from propath.
using OpenEdge.Web.IWebRequest from propath.
using OpenEdge.Net.HTTP.IHttpResponse from propath.
using Progress.Json.ObjectModel.JsonArray from propath.
using Progress.Json.ObjectModel.JsonObject from propath.
using com.sit.obp.ObjectFactory from propath.
using com.sit.CommonMessageLogger from propath.
using com.sit.obp.web.OBPWebHandler from propath.
using com.sit.obp.api.ClientNextOfKinWorkflow from propath.
using com.sit.obp.web.OBPDefaultWebResponse from propath.

class com.sit.obp.web.ClientNextOfKinOBPWebHandler inherits OBPWebHandler:

    /*------------------------------------------------------------------------------
    Purpose: Return a list of HTTP mehthods which are supported by this web handler
    Notes: Supports the OPTIONS verb, which is implemented in com.sit.obp.web.OBPWebHandler
    ------------------------------------------------------------------------------*/
    method override public character SupportedHTTPMethods( poRequest as IWebRequest ):
        if length(poRequest:GetPathParameter( "ClientNumber" ),"Character") > 0 then do:
            if length(poRequest:GetPathParameter( "NextOfKinClientNumber" ),"Character") > 0 then
                return "GET,PATCH,DELETE".
            else
                return "GET,POST".
        end.

        /* Required path parameters are missing, throw a com.sit.exceptions.ArgumentException for the base class to handle. */
        undo, throw new com.sit.exceptions.ArgumentException().
    end method.

    method override protected integer HandleGet( input poRequest as IWebRequest ):
        define variable oHttpResponse           as IHttpResponse no-undo.
        define variable oQueryArray             as JsonArray no-undo.
        define variable oQueryObject            as JsonObject no-undo.
        define variable nClientNumber           as integer no-undo.
        define variable nNextOfKinClientNumber  as integer no-undo.
        define variable oWorkflow               as ClientNextOfKinWorkflow no-undo.
        define variable cRequiredParamValue     as character extent 1 no-undo.
        define variable cOptionalParamValue     as character extent 1 no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandleGet" ).

        assign oHttpResponse = OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:LogPathParameters( this-object:className, "HandleGet", "ClientNumber,NextOfKinClientNumber", poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1",poRequest:GetPathParameter( "ClientNumber" ))
                   cOptionalParamValue[1] = substitute("NextOfKinClientNumber,&1",poRequest:GetPathParameter( "NextOfKinClientNumber" ))
            .

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer(entry(2, cRequiredParamValue[1]))
                   nNextOfKinClientNumber = integer(entry(2, cOptionalParamValue[1])).

            if nClientNumber > 0 then
            do:
                oWorkflow = cast( ObjectFactory:New( get-class( ClientNextOfKinWorkflow)), ClientNextOfKinWorkflow ).

                // GetPathParameter returns empty string when parameter is missing
                if entry(2,cOptionalParamValue[1]) > "" then
                do:
                    /* If we've been provided a next of kin client number, then this is a request for a specific next of kin */
                    oQueryArray = oWorkflow:Query( nClientNumber, nNextOfKinClientNumber).

                    if valid-object(oQueryArray) and oQueryArray:length = 1 then do:
                        oQueryObject = oQueryArray:GetJsonObject(1).
                        this-object:mapNextOfKinObjectToURL( input OBPWebHandler:GetBaseURL( input poRequest ),
                                                             oQueryObject ).
                        oHttpResponse:Entity = oQueryObject.
                    end.
                    else
                        oHttpResponse = OBPDefaultWebResponse:response404.                    
                end.
                else do:
                    oQueryArray = oWorkflow:Query( nClientNumber ).
    
                    if valid-object( oQueryArray ) then
                    do:
                        if oQueryArray:Length > 0 then
                             this-object:mapNextOfKinArrayToURL( input OBPWebHandler:GetBaseURL( input poRequest ),
                                                                 input oQueryArray ).
    
                        oHttpResponse:Entity = oQueryArray.
                    end.
                    else
                        oHttpResponse:Entity = new JsonArray().
                end.
            end.
            else
                oHttpResponse = OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleGet" ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleGet":U, oErr ).
        end catch.
    end method.

    method override protected integer HandlePost( input poRequest as IWebRequest ):
        define variable oHttpResponse           as IHttpResponse no-undo.
        define variable oResponse               as JsonObject no-undo.
        define variable nClientNumber           as integer no-undo.
        define variable oWorkflow               as ClientNextOfKinWorkflow no-undo.
        define variable cRequiredParamValue     as character extent 1 no-undo.
        define variable cOptionalParamValue     as character extent   no-undo.
        define variable oRequestBody            as JsonObject no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePost" ).

        assign oHttpResponse = OBPWebHandler:GetDefaultResponse()
               oRequestBody = cast( poRequest:Entity, JsonObject )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePost":U, "ClientNumber":U, poRequest ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U,poRequest:GetPathParameter( "ClientNumber" )).

            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign nClientNumber = integer(entry(2,cRequiredParamValue[1])).

            if nClientNumber > 0 then
            do:
                oWorkflow = cast( ObjectFactory:New( get-class( ClientNextOfKinWorkflow)), ClientNextOfKinWorkflow ).

                oResponse = oWorkflow:Create(input nClientNumber, oRequestBody).
                if valid-object( oResponse ) then
                    assign oHttpResponse:Entity = oResponse
                           oHttpResponse:StatusCode = integer( StatusCodeEnum:Created )
                    .
                else
                    oHttpResponse = OBPDefaultWebResponse:response404.
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePost" ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePost":U, oErr ).
        end catch.
    end method.

    method override protected integer HandlePatch( input poRequest as OpenEdge.Web.IWebRequest ):
        define variable oHttpResponse           as IHttpResponse no-undo.
        define variable oRequestBody            as JsonObject no-undo.
        define variable oWorkflow               as ClientNextOfKinWorkflow no-undo.
        define variable cRequiredParamValue     as character extent 2 no-undo.
        define variable cOptionalParamValue     as character extent no-undo.
        define variable nClientNumber           as integer no-undo.
        define variable nNextOfKinClientNumber  as integer no-undo.

        CommonMessageLogger:OperationStart( this-object:className, "HandlePatch" ).

        assign oHttpResponse = OBPWebHandler:GetDefaultResponse()
               oRequestBody = cast( poRequest:Entity, JsonObject )
        .

        CommonMessageLogger:LogPathParameters( this-object:className, "HandlePatch":U, "ClientNumber,NextOfKinClientNumber":U, poRequest ).

        do on error undo, throw: 
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U, poRequest:GetPathParameter( "ClientNumber" ))
                   cRequiredParamValue[2] = substitute("NextOfKinClientNumber,&1":U, poRequest:GetPathParameter( "NextOfKinClientNumber" ) )
            .
                   
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).
            
            assign
                nClientNumber = integer(entry(2,cRequiredParamValue[1]))
                nNextOfKinClientNumber = integer(entry(2,cRequiredParamValue[2]))
            .
            
            if nClientNumber > 0 and nNextOfKinClientNumber > 0 then
            do:
                /* Instantiate a new workflow object for every method invocation because the web handlers are cached */
                oWorkflow = cast( ObjectFactory:New( get-class( ClientNextOfKinWorkflow ) ), ClientNextOfKinWorkflow ).
                oHttpResponse:Entity = oWorkflow:Update( nClientNumber, nNextOfKinClientNumber, oRequestBody ).
            end.
            else
              oHttpResponse = OBPDefaultWebResponse:response400.
        
            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch. 
        end.

        OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandlePatch" ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandlePatch":U, oErr ).
        end catch.
    end method.
    
    method override protected integer HandleDelete( input poRequest as IWebRequest ):
        define variable oHttpResponse           as IHttpResponse no-undo.
        define variable nClientNumber           as integer no-undo.
        define variable nNextOfKinClientNumber  as integer no-undo.
        define variable oWorkflow               as ClientNextOfKinWorkflow no-undo.
        define variable cRequiredParamValue     as character extent 2 no-undo.
        define variable cOptionalParamValue     as character extent   no-undo.

        assign oHttpResponse = OBPWebHandler:GetDefaultResponse().

        CommonMessageLogger:OperationStart( this-object:className, "HandleDelete" ).

        do on error undo, throw:
            assign cRequiredParamValue[1] = substitute("ClientNumber,&1":U, poRequest:GetPathParameter( "ClientNumber" ))
                   cRequiredParamValue[2] = substitute("NextOfKinClientNumber,&1":U, poRequest:GetPathParameter( "NextOfKinClientNumber" ) )
            .
            
            this-object:ValidateParamValue(input cRequiredParamValue, input cOptionalParamValue).

            assign 
                nClientNumber = integer(entry(2,cRequiredParamValue[1]))
                nNextOfKinClientNumber = integer(entry(2,cRequiredParamValue[2]))
            .

            if nClientNumber > 0 and nNextOfKinClientNumber > 0 then
            do:
                oWorkflow = cast( ObjectFactory:New( get-class( ClientNextOfKinWorkflow)), ClientNextOfKinWorkflow ).

                oWorkflow:Delete( input nClientNumber, nNextOfKinClientNumber).

                /* We return a 204 here because the request provided has caused the successful deletion of the record.
                   This means that there's nothing to return to the client application.
                   Using HTTP 204 tells the client that this is the case. */
                oHttpResponse:StatusCode = integer( StatusCodeEnum:NoContent ).
            end.
            /* If no ClientNumber parameter is present, this is a bad request */
            else
                oHttpResponse = OBPDefaultWebResponse:response400.

            catch oErr as Progress.Lang.Error:
                oHttpResponse = OBPDefaultWebResponse:ExceptionToHttpResponse( oErr ).
            end catch.
        end.

        OBPWebHandler:WriteResponse( oHttpResponse ).

        this-object:setHttpResponse(oHttpResponse).

        CommonMessageLogger:OperationEnd( this-object:className, "HandleDelete" ).

        /* A response of 0 means that this handler will build the entire response;
           a non-zero value is mapped to a static handler in the webapp's /static/error folder.
           The mappings are maintained in the webapps's WEB-INF/web.xml
           A predefined set of HTTP status codes is provided in the OpenEdge.Net.HTTP.StatusCodeEnum
           enumeration */
        return 0.

        catch oErr as Progress.Lang.Error :
            if valid-object( this-object:txnLogger ) then
                this-object:txnLogger:OperationEndWithError( "9999":U, oErr ).

            CommonMessageLogger:OperationException(this-object:className, "HandleDelete":U, oErr ).
        end catch.
    end method.

    method private void mapNextOfKinArrayToURL( input pcBaseURL as character, input poNextOfKinArray as JsonArray ):
        define variable i as integer no-undo.

        do i = 1 to poNextOfKinArray:Length:
            this-object:mapNextOfKinObjectToURL( pcBaseURL, poNextOfKinArray:GetJsonObject( i )).
        end.
    end method.

    method private void mapNextOfKinObjectToURL( input pcBaseURL as character, input poNextOfKinObject as JsonObject ):
        define variable oLink as JsonArray  no-undo.
        define variable oHref as JsonObject no-undo.
        
        assign 
            oLink = new JsonArray()
            oHref = new JsonObject()
            .

        oHref:Add( "RelationType":U, "self" ).
        oHref:Add( "href":U, substitute( "&1/&2/Profile":U, pcBaseURL, poNextOfKinObject:GetInteger( "NextOfKinClientNumber" ) ) ).
        oLink:Add( oHref ).

        poNextOfKinObject:Add( "links":U, oLink ).
    end method.
end class.
